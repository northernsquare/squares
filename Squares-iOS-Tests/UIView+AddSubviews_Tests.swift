//
//  UIView+AddSubviews_Tests.swift
//  Squares-iOS-Tests
//
//  Created by Marcus Brissman on 2018-09-10.
//

import XCTest
import Squares

class UIView_AddSubviews_Tests: XCTestCase {

    func testAddSubviews() {

        let superview = UIView()

        let subview1 = UIView()
        let subview2 = UIView()

        superview.add(subviews: [subview1, subview2])

        XCTAssertEqual(superview.subviews.first, subview1)
        XCTAssertEqual(superview.subviews.last, subview2)
    }

    func testAddSubviewsAndTranslateAutoresizingMaskIntoConstraints() {

        let superview = UIView()

        let subview1 = UIView()
        let subview2 = UIView()

        superview.add(subviews: [subview1, subview2], translatesAutoresizingMaskIntoConstraints: false)

        XCTAssertEqual(superview.subviews.first, subview1)
        XCTAssertEqual(superview.subviews.last, subview2)

        XCTAssert(subview1.translatesAutoresizingMaskIntoConstraints == false)
        XCTAssert(subview2.translatesAutoresizingMaskIntoConstraints == false)
    }

    func testAddSubviewsAndNotTranslateAutoresizingMaskIntoConstraints() {

        let superview = UIView()

        let subview1 = UIView()
        let subview2 = UIView()

        superview.add(subviews: [subview1, subview2], translatesAutoresizingMaskIntoConstraints: true)

        XCTAssertEqual(superview.subviews.first, subview1)
        XCTAssertEqual(superview.subviews.last, subview2)

        XCTAssert(subview1.translatesAutoresizingMaskIntoConstraints == true)
        XCTAssert(subview2.translatesAutoresizingMaskIntoConstraints == true)
    }
}
