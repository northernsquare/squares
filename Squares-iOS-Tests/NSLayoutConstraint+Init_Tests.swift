//
//  NSLayoutConstraint+Init_Tests.swift
//
//  Created by Marcus Brissman on 2018-09-06.
//

import XCTest
import Squares

class NSLayoutConstraint_Init_Tests: XCTestCase {

    func testInitializeWithDifferentAttributes() {

        let firstItem = UIView()
        let secondItem = UIView()

        let constraint = NSLayoutConstraint(where: .top, of: firstItem, is: .equal, to: .bottom, of: secondItem)

        XCTAssertNotNil(constraint.firstItem, "First item is nil")
        XCTAssertNotNil(constraint.secondItem, "Second item is nil")

        XCTAssert(constraint.firstAttribute == .top, "First attribute is not expected")
        XCTAssert(constraint.secondAttribute == .bottom, "Second attribute is not expected")
        XCTAssert(constraint.relation == .equal, "Relationship attribute is not expected")
        XCTAssert(constraint.constant == 0, "Constant is not expected")
        XCTAssert(constraint.multiplier == 1, "Multiplier is not expected")
        XCTAssert(constraint.priority == .required, "Priority attribute is not expected")
    }

    func testInitializeWithOneAttribute() {

        let firstItem = UIView()
        let secondItem = UIView()

        let constraint = NSLayoutConstraint(where: .top, of: firstItem, is: .equal, to: secondItem)

        XCTAssertNotNil(constraint.firstItem, "First item is nil")
        XCTAssertNotNil(constraint.secondItem, "Second item is nil")

        XCTAssert(constraint.firstAttribute == .top, "First attribute is not expected")
        XCTAssert(constraint.secondAttribute == .top, "Second attribute is not expected")
        XCTAssert(constraint.relation == .equal, "Relationship attribute is not expected")
        XCTAssert(constraint.constant == 0, "Constant is not expected")
        XCTAssert(constraint.multiplier == 1, "Multiplier is not expected")
        XCTAssert(constraint.priority == .required, "Priority attribute is not expected")
    }

    func testInitializeWithHeight() {

        let view = UIView()

        let constraint = NSLayoutConstraint(
            where: .height,
            of: view,
            is: .equal,
            to: 10
        )

        XCTAssertNotNil(constraint.firstItem, "First item is nil")

        XCTAssertNil(constraint.secondItem, "Second item is not nil")

        XCTAssert(constraint.firstAttribute == .height, "First attribute is not expected")
        XCTAssert(constraint.secondAttribute == .notAnAttribute, "Second attribute is not expected")
        XCTAssert(constraint.relation == .equal, "Relationship attribute is not expected")
        XCTAssert(constraint.constant == 10, "Constant is not expected")
        XCTAssert(constraint.multiplier == 1, "Multiplier is not expected")
        XCTAssert(constraint.priority == .required, "Priority attribute is not expected")
    }

    func testInitializeWithWidth() {

        let view = UIView()

        let constraint = NSLayoutConstraint(
            where: .width,
            of: view,
            is: .equal,
            to: 10
        )

        XCTAssertNotNil(constraint.firstItem, "First item is nil")

        XCTAssertNil(constraint.secondItem, "Second item is not nil")

        XCTAssert(constraint.firstAttribute == .width, "First attribute is not expected")
        XCTAssert(constraint.secondAttribute == .notAnAttribute, "Second attribute is not expected")
        XCTAssert(constraint.relation == .equal, "Relationship attribute is not expected")
        XCTAssert(constraint.constant == 10, "Constant is not expected")
        XCTAssert(constraint.multiplier == 1, "Multiplier is not expected")
        XCTAssert(constraint.priority == .required, "Priority attribute is not expected")
    }

    func testInitializeWithRatio() {

        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false

        let ratioConstraint = NSLayoutConstraint(whereRatioOf: view, is: .equal, to: 0.5)

        XCTAssertNotNil(ratioConstraint.firstItem, "First item is nil")
        XCTAssertNotNil(ratioConstraint.secondItem, "Second item is nil")

        XCTAssert(ratioConstraint.firstAttribute == .height, "First attribute is not expected")
        XCTAssert(ratioConstraint.secondAttribute == .width, "Second attribute is not expected")
        XCTAssert(ratioConstraint.relation == .equal, "Relationship attribute is not expected")
        XCTAssert(ratioConstraint.constant == 0, "Constant is not expected")
        XCTAssert(ratioConstraint.multiplier == 0.5, "Multiplier is not expected")
        XCTAssert(ratioConstraint.priority == .required, "Priority attribute is not expected")

        // Test to create a view
        let widthConstraint = NSLayoutConstraint(where: .width, of: view, is: .equal, to: 300)

        ratioConstraint.isActive = true
        widthConstraint.isActive = true

        UIWindow().addSubview(view)
        view.layoutIfNeeded()
        
        XCTAssert(view.bounds.height == 150)
    }
}
