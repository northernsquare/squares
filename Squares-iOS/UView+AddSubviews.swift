//
//  UView+AddSubviews.swift
//  Squares-iOS
//
//  Created by Marcus Brissman on 2018-09-10.
//

public extension UIView {

    func add(subviews: [UIView]) {

        subviews.forEach { view in
            addSubview(view)
        }
    }

    func add(subviews: [UIView], translatesAutoresizingMaskIntoConstraints: Bool) {

        add(subviews: subviews)

        subviews.forEach { view in
            view.translatesAutoresizingMaskIntoConstraints = translatesAutoresizingMaskIntoConstraints
        }
    }
}
