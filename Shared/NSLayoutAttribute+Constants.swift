//
//  NSLayoutAttribute+Constants.swift
//  Should
//
//  Created by Marcus Brissman on 2018-01-19.
//  Copyright © 2018 Northern Square. All rights reserved.
//

public extension Array where Element == NSLayoutConstraint.Attribute {

    // Edges
    static var allEdges: [Element] {
        return [.top, .trailing, .bottom, .leading]
    }

    static var horizontalEdges: [Element] {
        return [.trailing, .leading]
    }

    static var topAndHorizontalEdges: [Element] {
        return [.top, .trailing, .leading]
    }

    static var bottomAndHorizontalEdges: [Element] {
        return [.trailing, .bottom, .leading]
    }

    static var verticalEdges: [Element] {
        return [.top, .bottom]
    }

    // Center
    static var center: [Element] {
        return [.centerX, .centerY]
    }

    // Corners
    static var trailingBottomCorner: [Element] {
        return [.trailing, .bottom]
    }
}
