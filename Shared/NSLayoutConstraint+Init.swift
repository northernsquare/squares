//
//  NSLayoutConstraints+Convenience.swift
//  Should
//
//  Created by Marcus Brissman on 2018-01-19.
//  Copyright © 2018 Northern Square. All rights reserved.
//

public extension NSLayoutConstraint {

    /// Creates a constraint that defines the relationship between two layoutable items.
    convenience init(
        where fromAttribute: NSLayoutConstraint.Attribute,
        of fromItem: AnyObject,
        is relatedBy: NSLayoutConstraint.Relation,
        to toAttribute: NSLayoutConstraint.Attribute,
        of toItem: AnyObject,
        constant: CGFloat = 0) {

        self.init(
            item: fromItem,
            attribute: fromAttribute,
            relatedBy: relatedBy,
            toItem: toItem,
            attribute: toAttribute,
            multiplier: 1,
            constant: constant
        )
    }

    /// Creates a constraint that defines the relationship between two layoutable items.
    convenience init(
        where attribute: NSLayoutConstraint.Attribute,
        of fromItem: AnyObject,
        is relatedBy: NSLayoutConstraint.Relation,
        to toItem: AnyObject) {

        self.init(
            item: fromItem,
            attribute: attribute,
            relatedBy: relatedBy,
            toItem: toItem,
            attribute: attribute,
            multiplier: 1,
            constant: 0
        )
    }

    /// Creates a constraint that defines the dimension of a layoutable item related to a constant.
    convenience init(
        where dimension: NSLayoutConstraint.Dimension,
        of item: AnyObject,
        is relatedBy: NSLayoutConstraint.Relation,
        to constant: CGFloat) {

        self.init(
            item: item,
            attribute: dimension.attribute,
            relatedBy: relatedBy,
            toItem: nil,
            attribute: .notAnAttribute,
            multiplier: 1,
            constant: constant
        )
    }

    /// Creates a constraint that defines the ratio of a layoutable item.
    convenience init(
        whereRatioOf item: AnyObject,
        is related: NSLayoutConstraint.Relation,
        to ratio: CGFloat) {

        self.init(
            item: item,
            attribute: .height,
            relatedBy: related,
            toItem: item,
            attribute: .width,
            multiplier: ratio,
            constant: 0
        )
    }
}
