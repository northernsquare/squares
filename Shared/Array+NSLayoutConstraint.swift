//
//  Array+NSLayoutConstraint.swift
//  Squares-macOS
//
//  Created by Marcus Brissman on 2018-09-05.
//

public extension Array where Element == NSLayoutConstraint {

    /// Appends a constraint where an attribute of a layoutable item is related to
    /// another attribute a layoutable item.
    mutating func append(
        where fromAttribute: NSLayoutConstraint.Attribute,
        of fromItem: AnyObject,
        is relatedBy: NSLayoutConstraint.Relation,
        to toAttribute: NSLayoutConstraint.Attribute,
        of toItem: AnyObject,
        constant: CGFloat = 0) {

        let constraint = NSLayoutConstraint(
            where: fromAttribute,
            of: fromItem,
            is: relatedBy,
            to: toAttribute,
            of: toItem,
            constant: constant
        )

        append(constraint)
    }

    /// Appends a constraint where one attribute of two layoutable items is related to the each other.
    mutating func append(
        where attribute: NSLayoutConstraint.Attribute,
        of item: AnyObject,
        is relation: NSLayoutConstraint.Relation,
        to anotherItem: AnyObject) {

        let constraint = NSLayoutConstraint(where: attribute, of: item, is: relation, to: anotherItem)

        append(constraint)
    }

    /// Appends a constraint where the dimension of an item is related to a constant.
    mutating func append(
        where dimension: NSLayoutConstraint.Dimension,
        of item: AnyObject,
        is relatedBy: NSLayoutConstraint.Relation,
        to constant: CGFloat) {

        let constraint = NSLayoutConstraint(where: dimension, of: item, is: relatedBy, to: constant)

        append(constraint)
    }

    /// Appends a constraint that defines the ratio of a layoutable item.
    mutating func append(whereRatioOf item: AnyObject, is related: NSLayoutConstraint.Relation, to ratio: CGFloat) {

        let constraint = NSLayoutConstraint(whereRatioOf: item, is: related, to: ratio)

        append(constraint)
    }

    /// Appends constraints where attributes of a layoutable item is related to another layoutable item.
    mutating func append(
        where attributes: [NSLayoutConstraint.Attribute],
        of fromItem: AnyObject,
        is relatedBy: NSLayoutConstraint.Relation,
        to toItem: AnyObject) {

        attributes.forEach { attribute in
            append(where: attribute, of: fromItem, is: relatedBy, to: attribute, of: toItem)
        }
    }

    /// Appends constraints defining the size of a layoutable item related to a instance of `CGSize`.
    mutating func append(
        whereSizeOf item: AnyObject,
        is related: NSLayoutConstraint.Relation,
        to size: CGSize) {

        append(where: .width, of: item, is: related, to: size.width)
        append(where: .height, of: item, is: related, to: size.height)
    }

    /// Appends constraints to make sure a layoutable item is within the bounds of another layoutable item.
    mutating func append(where item: AnyObject, isWithin anotherItem: AnyObject) {

        append(where: [.top, .left], of: item, is: .greaterThanOrEqual, to: anotherItem)
        append(where: [.bottom, .right], of: item, is: .lessThanOrEqual, to: anotherItem)
    }

    /// Activates constraints.
    func activate() {
        forEach { constraint in
            constraint.isActive = true
        }
    }

    /// Deactivates constraints.
    func deactivate() {
        forEach { constraint in
            constraint.isActive = false
        }
    }
}
