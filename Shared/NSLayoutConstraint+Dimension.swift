//
//  NSLayoutConstraint+Dimension.swift
//  Squares-macOS
//
//  Created by Marcus Brissman on 2018-09-06.
//

public extension NSLayoutConstraint {

    enum Dimension {

        case width
        case height

        var attribute: NSLayoutConstraint.Attribute {
            switch self {
            case .width:
                return .width
            case .height:
                return .height
            }
        }
    }
}
